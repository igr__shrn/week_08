package ru.edu.filter;

import org.junit.Before;
import org.junit.Test;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

public class LoginFilterTest {
    private static final String ENTER_SUCCESS = "index?enter=success";
    private static final String ENTER_FAIL = "login?enter=fail";

    private FilterChain filterChain = mock(FilterChain.class);
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);

    private LoginFilter loginFilter;

    @Before
    public void setup() {
        loginFilter = new LoginFilter();
    }

    @Test
    public void testDoFilter() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("/index");

        when(request.getParameter("login")).thenReturn("admin");
        when(request.getParameter("password")).thenReturn("admin");

        loginFilter.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    public void testDoFilterGetMethod() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("login");
        when(request.getMethod()).thenReturn("GET");

        loginFilter.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    public void testDoFilterLoginWithTruePass() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("login");
        when(request.getMethod()).thenReturn("POST");

        when(request.getParameter("login")).thenReturn("admin");
        when(request.getParameter("password")).thenReturn("admin");

        loginFilter.doFilter(request, response, filterChain);

        verify(response, times(1)).sendRedirect(ENTER_SUCCESS);
    }

    @Test
    public void testDoFilterLoginWithoutTruePass() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("login");
        when(request.getMethod()).thenReturn("POST");

        when(request.getParameter("login")).thenReturn("adminTmp");
        when(request.getParameter("password")).thenReturn("adminTmp");

        loginFilter.doFilter(request, response, filterChain);

        verify(response, times(1)).sendRedirect(ENTER_FAIL);
    }

}