package ru.edu.filter;

import org.junit.Before;
import org.junit.Test;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.time.Instant;

import static org.mockito.Mockito.*;

public class AuthFilterTest {
    private static final String AUTH_COOKIE_NAME = "authCookie";

    private FilterChain filterChain = mock(FilterChain.class);

    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);

    private AuthFilter auth;

    @Before
    public void setup() {
        auth = new AuthFilter();
    }

    @Test
    public void testDoFilterIndex() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("/index");

        auth.doFilter(request, response, filterChain);

        verify(request, times(1)).setAttribute("auth", false);
        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    public void testDoFilterIndexWithCookie() throws ServletException, IOException {
        Cookie[] cookies = {new Cookie(AUTH_COOKIE_NAME,
                Long.toString(Instant.now().getEpochSecond()))};

        when(request.getCookies()).thenReturn(cookies);
        when(request.getServletPath()).thenReturn("/index");

        auth.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    public void testDoFilterView() throws ServletException, IOException {
        when(request.getServletPath()).thenReturn("/view");

        auth.doFilter(request, response, filterChain);

        verify(request, times(1)).setAttribute("auth", false);
    }

    @Test
    public void testDoFilterEditWithCookie() throws ServletException, IOException {
        Cookie[] cookies = {new Cookie(AUTH_COOKIE_NAME,
                Long.toString(Instant.now().getEpochSecond()))};

        when(request.getServletPath()).thenReturn("/edit");
        when(request.getCookies()).thenReturn(cookies);

        auth.doFilter(request, response, filterChain);

        verify(filterChain, times(1)).doFilter(request, response);
    }

    @Test
    public void testDoFilterEditWithoutCookie() throws ServletException, IOException {
        Cookie[] cookies = {};

        when(request.getServletPath()).thenReturn("/edit");
        when(request.getCookies()).thenReturn(cookies);

        auth.doFilter(request, response, filterChain);

        verify(response, times(1)).sendError(HttpServletResponse.SC_FORBIDDEN,
                "Not authorized");
    }
}