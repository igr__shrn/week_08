package ru.edu.filter;

import javax.servlet.Filter;
import javax.servlet.ServletException;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.FilterChain;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;

public class LoginFilter implements Filter {
    /**
     * Наименование cookie для авторизации.
     */
    public static final String AUTH_COOKIE_NAME = "authCookie";

    /**
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {

    }

    /**
     * @param request
     * @param response
     * @param chain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(final ServletRequest request,
                         final ServletResponse response,
                         final FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpReq = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        if (!httpReq.getServletPath().contains("login")
                || "GET".equals(httpReq.getMethod())) {
            chain.doFilter(request, response);
            return;
        }

        String login = httpReq.getParameter("login");
        String password = httpReq.getParameter("password");

        if (login.equals("admin") && password.equals(login)) {
            Cookie authCookie = new Cookie(AUTH_COOKIE_NAME,
                    Long.toString(Instant.now().getEpochSecond()));
            authCookie.setPath("/");
            httpResponse.addCookie(authCookie);
            httpResponse.sendRedirect("index?enter=success");
        } else {
            httpResponse.sendRedirect("login?enter=fail");
        }
    }

    /**
     *
     */
    @Override
    public void destroy() {

    }
}
