<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.edu.db.Advert" %>
<html>
<head>
    <jsp:include page="/WEB-INF/templates/header.jsp">
        <jsp:param name="title" value="Index"/>
    </jsp:include>
</head>
<body>
<div class="page">
    <div class="header">
        <h1>Доска объявлений</h1>
    </div>

    <%
        if (request.getAttribute("error") != null) {
            String error = (String) request.getAttribute("error");
            if (error.equals("id_not_found")) { %>
    <div class="error">Оъявление по данному ID не найдено!</div>
    <% }
        if (error.equals("id_param_missing")) { %>
    <div class="error">Потерян GET параметр ID!</div>
    <% }
    }
    %>
    <%@ include file="/WEB-INF/templates/breadcrumbs.jsp" %>
    <div class="content">
        <%
            List<Advert> adverts = (List<Advert>) request.getAttribute("adverts");
            if (adverts.isEmpty()) { %>
        <h2>Записей нет</h2>
        <% } else {
            for (Advert adv : adverts) {
        %>
        <div class="advert">
            <div class="img"><img src="<%=adv.getPictureUrl()%>?t=<%=java.time.Instant.now().getNano()+adv.getId()%>"/>
            </div>
            <div class="title"><%=adv.getTitle()%>
            </div>
            <div class="type"><%=adv.getType()%>
            </div>
            <div class="short-description"><%=adv.getShort(300)%>
            </div>
            <div class="price"><%=adv.getPrice().toPlainString()%>
            </div>
            <div class="links">
                <a href="view?id=<%=adv.getId()%>">Просмотр</a>
                <% if (request.getAttribute("auth").equals(true)) { %>
                | <a href="edit?id=<%=adv.getId()%>">Редактировать</a>
                <% } %>
            </div>
        </div>
        <% }
        } %>
    </div>
    <% if (request.getAttribute("auth").equals(true)) { %>
    <div class="button-container">
        <a href="edit">
            <span class="send-button">Разместить объявление</span>
        </a>
    </div>
    <% } %>
    <%@ include file="/WEB-INF/templates/footer.jsp" %>
</div>
</body>
</html>